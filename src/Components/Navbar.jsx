import React from "react";
import { Link } from "react-router-dom";
import CustomLink from "./CustomLink";
import "../assets/css/navbar.css";

const Navbar = () => {
    return (
        <ul className="nav nav-tabs justify-content-center">
            <li className="nav-item nav-tabbing nav-backlog">
                {/* Added custom link component for active link matching */}
                <CustomLink className="nav-link" to="/backlog">
                    Backlog
                </CustomLink>
            </li>
            <li className="nav-item nav-tabbing nav-today">
                <CustomLink className="nav-link" to="/">
                    Today
                </CustomLink>
            </li>
            <li className="nav-item nav-tabbing">
                <Link className="nav-link" to="/user">
                    <i className="fa-solid fa-circle-user"></i>
                </Link>
            </li>
        </ul>
    );
};

export default Navbar;
