import React, { useEffect, useRef, useState } from "react";
import Overdue from "./Overdue";
import { randomId } from "../helper/helper"; // imported helper function for random ID
import "../assets/css/todo.css";
import { useLocation } from "react-router-dom";

const Todo = () => {
    const location = useLocation();
    const todoPage = useRef(null);

    const input = {
        id: "",
        inp: "",
        isChecked: false,
        isEditing: true,
    };

    const [getTodo, setGetTodo] = useState([
        {
            id: randomId(),
            inp: "Implement sorting",
            isChecked: true,
            isEditing: false,
        },
        {
            id: randomId(),
            inp: "Implement authentication",
            isChecked: true,
            isEditing: false,
        },
        {
            id: randomId(),
            inp: "Post progress on Twitter",
            isChecked: true,
            isEditing: false,
        },
        // all todos added to this state
    ]);

    const handleAddTask = (e, i) => {
        let list = getTodo;
        let current = list[i];
        current.inp = e.target.value;
        current.id = randomId();
        list[i] = current;
        setGetTodo([...list]);
        // gets state of latest task added and sets the value and ID of the task added, then appends the value to the getTodo array
    };

    const handleBlur = (i) => {
        let list = getTodo;
        let current = list[i];
        current.isEditing = false;
        list[i] = current;
        setGetTodo([...list]);
        // gets state of latest task added and sets isEditing to false so that input tag gets changed to span tag
    };

    const handleTaskStatus = (i) => {
        setGetTodo(
            getTodo.map((el) => {
                if (el.id === i) {
                    return { ...el, isChecked: !el.isChecked };
                }
                return el;
                // function to change checked state of selected task
            }),
        );
    };

    const handlePageAnimation = () => {
        if (location.pathname === "/") {
            todoPage.current.classList.add("page-fade");
        } else {
            todoPage.current.classList.remove("page-fade");
        }
        // added custom CSS for transition when component is loaded
    };

    useEffect(() => {
        handlePageAnimation();
    }, [location]);

    return (
        <>
            <Overdue />
            <div className="todo-page" ref={todoPage}>
                <h2>Today</h2>
                <div className="todo-wrapper page">
                    <ul className="list-wrapper">
                        {getTodo.map((e, i) => {
                            return (
                                <li
                                    key={e.id}
                                    onClick={() => handleTaskStatus(e.id)}
                                    className={
                                        e.isChecked === true ? "checked" : ""
                                        // apply checked class for line-through if task is checked
                                    }>
                                    <i
                                        className={`fa-solid ${
                                            e.isChecked === true
                                                ? "fa-check"
                                                : "fa-circle"
                                            // change class if task is checked
                                        }`}></i>

                                    {e.isEditing ? (
                                        <input
                                            type="text"
                                            className="task-input"
                                            value={e.inp}
                                            onChange={(e) => {
                                                console.log(e.target.value);
                                                handleAddTask(e, i);
                                            }}
                                            onBlur={() => handleBlur(i)}
                                            autoFocus
                                        />
                                    ) : (
                                        <span>{e.inp}</span>
                                    )}
                                </li>
                            );
                        })}
                        <form
                            onSubmit={(e) => {
                                e.preventDefault();
                                setGetTodo([...getTodo, input]);
                            }}>
                            <div className="task-wrapper">
                                <i className="fa-solid fa-plus"></i>
                                <button
                                    type="submit"
                                    className="task-btn"
                                    onClick={() => {}}>
                                    Add Task
                                </button>
                            </div>
                        </form>
                    </ul>
                </div>
                <div className="finish-btn-wrapper">
                    <p>Ready to finish your day?</p>
                    <button className="btn btn-outline-light">
                        Finish Today
                    </button>
                </div>
            </div>
        </>
    );
};

export default Todo;
