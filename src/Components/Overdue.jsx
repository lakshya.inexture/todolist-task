import React, { useState } from "react";
import "../assets/css/overdue.css";

const Overdue = () => {
    const [moreTasks, setMoreTasks] = useState(false);
    // state to display and hide tasks

    const overDueTasks = [
        "Prepare lunch for weekdays",
        "Do the laundry",
        "Implement sorting",
        "Implement authentication",
        "Post progress on Twitter",
    ];

    const slicedTasks = overDueTasks.slice(0, 2);
    // slice tasks

    return (
        <div className="overdue">
            <div className="heading">
                <h2>Overdue</h2>
                <button
                    className="btn btn-primary"
                    onClick={() => setMoreTasks(false)}>
                    Finish Overdue
                </button>
            </div>
            <ul className="list-wrapper">
                {/* default behaviour is to display sliced tasks, if clicked on '3 more tasks', displays all tasks and if clicked on finish overdue button, displays sliced tasks again */}
                {!moreTasks
                    ? slicedTasks.map((e, i) => {
                          return (
                              <li key={i}>
                                  <i className="fa-solid fa-circle"></i>
                                  {e}
                              </li>
                          );
                      })
                    : overDueTasks.map((e, i) => {
                          return (
                              <li key={i}>
                                  <i className="fa-solid fa-circle"></i>
                                  {e}
                              </li>
                          );
                      })}
                {!moreTasks && (
                    <li onClick={() => setMoreTasks(true)}>
                        <i className="fa-solid fa-ellipsis"></i>3 more tasks
                    </li>
                )}
            </ul>
        </div>
    );
};

export default Overdue;
