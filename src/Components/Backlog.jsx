import React, { useEffect, useRef } from "react";
import Overdue from "./Overdue";
import "../assets/css/backlog.css";
import { useLocation } from "react-router-dom";

const Backlog = () => {
    const backLog = useRef(null);
    const location = useLocation();

    const handlePageAnimation = () => {
        console.log(location.pathname);

        if (location.pathname === "/backlog") {
            backLog.current.classList.add("page-fade");
        } else {
            backLog.current.classList.remove("page-fade");
        }
        // added custom CSS for transition when component is loaded
    };

    useEffect(() => {
        handlePageAnimation();
    }, [location]);

    return (
        <>
            <Overdue />
            <div className="backlog" ref={backLog}>
                <div className="container-fluid backlog-wrapper">
                    <i className="fa-solid fa-cloud-sun"></i>
                    <p>Your backlog is empty.</p>
                </div>
            </div>
        </>
    );
};

export default Backlog;
