import React, { useEffect, useRef } from "react";
import { useLocation } from "react-router-dom";
import "../assets/css/user.css";

const User = () => {
    const user = useRef(null);
    const location = useLocation();

    const handlePageAnimation = () => {
        if (location.pathname === "/user") {
            user.current.classList.add("page-fade");
        } else {
            user.current.classList.remove("page-fade");
        }
        // added custom CSS for transition when component is loaded
    };

    useEffect(() => {
        handlePageAnimation();
    }, [location]);

    return (
        <div className="user-wrapper" ref={user}>
            <h2>Profile</h2>
            <div className="container user-container">
                <p>Email: email@ondrejbarta.xyz</p>
                <p>UID: 8RjbWdloWjPcfXX5Scj4Q2bAdxt2</p>
            </div>
            <button>Logout</button>
        </div>
    );
};

export default User;
