import React from "react";
import { Link, useMatch, useResolvedPath } from "react-router-dom";

const CustomLink = ({ children, to, ...props }) => {
    let resolved = useResolvedPath(to);
    let match = useMatch({ path: resolved.pathname, end: true });
    // checks for path of current page endpoint and matches against current link

    return (
        <div>
            <Link
                style={{
                    backgroundColor: match ? "gray" : "lightgray",
                    color: match ? "white" : "black",
                    border: "none",
                    // custom CSS given for active link if matched path true
                }}
                to={to}
                {...props}>
                {children}
            </Link>
            {match && ""}
        </div>
    );
};

export default CustomLink;
