import { Route, Routes } from "react-router-dom";
import "./App.css";
import Backlog from "./Components/Backlog";
import Navbar from "./Components/Navbar";
import Todo from "./Components/Todo";
import User from "./Components/User";

function App() {
    return (
        <>
            <Navbar />
            <Routes>
                <Route path="/" element={<Todo />} />
                <Route path="/user" element={<User />} />
                <Route path="/backlog" element={<Backlog />} />
            </Routes>
        </>
    );
}

export default App;
